<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from www.quickfreightsolutions.com/contact by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 30 May 2023 19:43:59 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head><!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'../www.googletagmanager.com/gtm5445.html?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5ZZ2CFJ');</script>
<!-- End Google Tag Manager -->

<title>Shipping Company Hackettstown, Vancouver | Fast Moving Courier</title>

<meta charset="utf-8">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="Description" content="Call 520-442-3243 for reliable, ecomonical shipping company, Delivery based in Hackettstown, Vancouver BC." />
<meta name="Keywords" content="Contact" />
<meta name="Author" content="Cityline Websites">
<!-- responsive -->
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<base  />

<link rel="stylesheet" type="text/css" href="template/css/basic144e.css?1530825841" media="all, handheld" />
<link rel="stylesheet" type="text/css" href="template/css/navigationf234.css?1530825842" media="all, handheld" />

<!-- js config files -->
<script type='text/javascript'>
      var config = new Array();
      config.site_path = "index.html";
      config.admin_url = "https://fast.movcourier.com/admin/";
</script>

<!-- load resources -->
<script type="text/javascript" src="js/lazyload.js"></script>
<script type="text/javascript" src="js/jquery/jquery-1.11.3.min.js"></script>
<script type="text/javascript" src="js/jquery/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript" src="js/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
<script>
      var standardcss = ["https://fast.movcourier.com/template/css/responsive-tables.css","js/fancybox/jquery.fancybox-1.3.4.css","//fonts.googleapis.com/css?family=Lato:400,700,900","https://fast.movcourier.com/template/modules/banners/css/banner.css"];
      var standardjs = ["https://fast.movcourier.com/js/responsive-tables.js","js/navigation.js","js/respond.min.js","js/Placeholders.min.js","https://fast.movcourier.com/js/functions.js"];
      LazyLoad.css(standardcss);
      LazyLoad.js(standardjs);
</script>

<!-- Google Tag Manager 
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5ZZ2CFJ');</script>
End Google Tag Manager -->


      <script type="text/javascript">
          $(document).ready(function(){
      		$("img").bind("contextmenu",function(e){
      			return false;
      		});
          });
      </script>


</head>
<body><!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5ZZ2CFJ"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<!-- in case of missing js -->
<noscript>
	<!-- Standard CSS files -->
	
	<div class="nojs" style="padding-left: 1em;">Use javascript to get the most from this site.</div>
</noscript>
<!-- -->
<!-- ********************* START TOP ********************* -->
<div id="top">

	<!-- ** ADD ICON BAR **  -->
	<link href="template/iconbar/iconbar.css" rel="stylesheet">
<div id="iconbar">
	<div class="wrap" style="position:relative;background:transparent;">

		

		<!-- ############################################################# -->

				
		
		
		
		<div class="topEmail"><a href="Send%20me%20a%20message%21.html" onclick="str='mai'+'lto:i'+'nfox'+'quickfreightsolutions.c'+'om';this.href=str.replace('x','@')"><img src="template/iconbar/icons/top-email-icon.png" alt="contact us" /><span class="topEmailUs">info&#x40;movcourier.com<span style="unicode-bidi:bidi-override;direction:rtl; font-weight:400;">moc.</span></span><span class="mobileEmailUs">Email Us</span></a></div>
		
		<div class="callNow">
		<div class="callNowContent"><a href="tel:1.520-442-3243"><img src="template/iconbar/icons/top-phone-icon.png" alt="call us" /><span class="callUsDesktop">520-442-3243</span><span class="callUsMobile">Call Us</a></div>
		</div>
		
		
		
		
		
		

	</div>
</div>

	<!-- ** ADD HEADER **  -->
	
		
<div id="header">

<div class="wrap">


<div id="header-logo">
  <a href="index.html" title="Fast Moving Courier - Home">
    <img src="images/logo-quickfreightsolutions.png"  alt="Fast Moving Courier">
  </a><!-- header_logo -->
</div>

<div class="headerRight">





<!-- ** ADD TOP MENU ** //  -->

<div id="topNav"><!-- start mainmenu -->

	<div id="mobileMenu">
		<!--prepended text-->
		<div class='hamburger'><i></i><i></i><i></i></div>
		<!--appended text-->
	</div>

	<nav>
	<div class="relative_topMenu">
	<ul id="topMenu" class="topRowUL notMobile">
	<li class='topRowLI   '><a class='topRowA   ' href='index.html'>Home</a></li><li class='topRowLI   '><a class='topRowA   ' href='services.html'>Services</a></li><li class='topRowLI   '><a class='topRowA   ' href='shippers.html'>Shippers</a></li><li class='topRowLI   '><a class='topRowA   ' href='carriers.html'>Carriers</a></li><li class='topRowLI   '><a class='topRowA   ' href='philosophy.html'>Philosophy</a></li><li class='topRowLI   '><a class='topRowA   ' href='faq-s.html'>FAQ</a></li><li class='topRowLI markedGroup thisIsThePage '><a class='topRowA markedGroup thisIsThePage ' href='contact.html'>Contact</a></li>	</ul><!-- end top menu -->
	</div>
	</nav>


</div>
<script>
	/* set mobile menu button text to page title */
	var newmenutext = "Menu";
	//$('#mobileMenu').html(newmenutext); // replace text
	$('#mobileMenu').append(newmenutext); // add after existing text
	//$('#mobileMenu').prepend(newmenutext); // add before existing text

	
	var jqTopPlus="&#9660;";
	var jqTopMinus="&#9650;";
	var jqSubPlus="&#9660;";
	var jqSubMinus="&#9650;";
	var jqSidePlus="&#9660;";
	var jqSideMinus="&#9650;";

	var jqMobTopPlus="&#9660;";
	var jqMobTopMinus="&#9650;";
	var jqMobSubPlus="&#9660;";
	var jqMobSubMinus="&#9650;";
	var jqMobSidePlus="&#9660;";
	var jqMobSideMinus="&#9650;";
	</script>

 <!-- END mainmenu -->
<div style="clear:both;"></div>

</div><!--header right container-->


<div style="clear:both"></div>

</div><!--close wrap-->

		<div id="inside-banner" class="wrap">
			<img src="uploads/banners/banner/bannerinside1.jpg" alt="Quick Freight Banner"/>
		</div><!-- end inside-banner -->
		<div style="clear:both"></div>
	
<div style="clear:both;"></div>


</div><!-- header -->
	

</div> <!-- top -->
<!-- ********************* END OF TOP ********************* -->

<!-- ********************* START MIDDLE ********************* -->

<div id="middle">
<!-- ************* START OF CONTENT AREA ********************* -->


<div id="content_area" class="wrap">

<div id="contentContact">

<h1>Get In Touch With Us</h1>
<p>If you are looking for a trouble-free shipment with a reliable shipping company, please contact Fast Moving Courier.</p>
<p></p>
<p><strong>Fast Moving Courier</strong><br />349 Blau Road, Hackettstown<br> New Jersey, 07840<br />US</p>
<p>Telephone: <a href="tel:1.520-442-3243">520-442-3243</a></p>
<p>Please take a minute to <a href="https://search.google.com/local/writereview?placeid=ChIJeRyq3JPPhVQRcUVIHPZuKis" target="_blank" title="review">leave us a review</a>!</p>  


	



</div><!--content contact-->



<div id="sidebarContact">
        <!--sidebar info here-->



<div id="inquiry">
<a name="emailUs"></a>
	<div><h3 color="blue">Email Recieved!!!</h3></div>
<div class="connectHeader">Request Quote</div>

			<div class="ajaxForm" data-location='https://fast.movcourier.com/external/contact_form.php' data-post='{"errors":false,"post":[],"sent":0,"referrer":"\/contact"}' ></div>
		<!-- <p class="required">* Required</p> -->

	</div>


	</div> <!--sidebar-->







<div style="clear: both;"></div>


		
		
		
		
		
		
		
		

</div>	<!-- content_area -->
	<div style="clear:both"></div>

<!-- ************* END OF CONTENT AREA ********************* -->

</div>
<!-- ********************* END MIDDLE ********************* -->


<div class="topBannerContainer">
<div class="wrap">


<div class="needToShipSomething">Need to ship something?</div>
<a href="contact.html"><div class="requestQuoteButton">Request a Shipping Quote</div></a>
<div class="callUsTwoFourSeven">Call us 24/7/365</div>
<div class="topBannerCallUsIcon"><img src="images/icon-banner-phone.png" alt="call us" /></div>
<div class="topBannerCallUs"><a href="tel:1.520-442-3243">520-442-3243</a></div>


<div class="topBannerContent">Need to ship something? <br /> &nbsp; Call us 24/7/365 &nbsp; <img src="images/icon-banner-phone.png" alt="call us" /> <a href="tel:1.520-442-3243">520-442-3243</a></div><!--banner content-->

</div><!--close wrap-->
</div><!--the banner container 100% bg-->

<div style="clear:both"></div>

<div id="bottom">

<!-- ************* START OF FOOTER ********************* -->
	<div id="footer" class="wrap">
		<div id="footer-col1">
		
            	<h3>Our Philosophy</h3>
                
				<p>FMS is powered by industry leading technology and a commitment to your business. If you need it done, trust it will get done.</p>
				
				<h3 class="extraMargin">Connect With Us</h3>
				<p>Call For A Quote: <a href="tel:+1.520-442-3243">+1.520-442-3243</a></p>
				<p>Email Us: <a href="Send%20me%20a%20message%21.html" onclick="str='mai'+'lto:i'+'nfox'+'quickfreightsolutions.c'+'om';this.href=str.replace('x','@')">info&#x40;movcourier.com<span style="unicode-bidi:bidi-override;direction:rtl; font-weight:700;">moc.</span></a></p>
				<p>Read <a href="blog.html">Our Blog</a></p>
				
								
				
				
				
		</div> <!-- section1 -->

		<div id="footer-col2">
		
			<h3>Useful Links</h3>
			
        		<p><a href="index.html">&gt;&nbsp;Home</p>
                <p><a href='services.html' >&gt;&nbsp;Services</a><br /></p>
                <p><a href='shippers.html' >&gt;&nbsp;Shippers</a><br /></p>
                <p><a href='carriers.html' >&gt;&nbsp;Carriers</a><br /></p>
                <p><a href='philosophy.html' >&gt;&nbsp;Philosophy</a><br /></p>
				<p><a href='faq-s.html' >&gt;&nbsp;FAQ&rsquo;s</a><br /></p>
                <p><a href='contact.html' rel=\"nofollow\">&gt;&nbsp;Contact</a><br /></p>
				<p><a href="site-map.html">&gt;&nbsp;Site Map</a></p>
				
				
		</div> <!-- section -->

        <div id="footer-col3">
                
				<h3>Organizations</h3>
				
				<div class="footerOrgLogo"><a href="http://smartwaytrans.com/wp/" target="_blank"><img src="images/logo-smartway2.png" alt="SmartWay Logo" /></a></div>
				<div class="footerOrgLogo"><a href="http://www.tianet.org/" target="_blank"><img src="images/tia-logo.png" alt="TIA Logo" /></a></div>
				
				

		</div> <!-- section -->
		
		

<div style="clear:both"></div>



<a href="javascript:window.scrollTo(0,0);" title="Back To Top" id="backtotop"><div class="upArrow"><img src="images/upArrow.png" alt="go up" /></div></a>

<div style="clear:both"></div>

<script>
			$(document).ready(function($){
				$('#backtotop').click(function(){
					$("html, body").animate({ scrollTop: 0 }, 500);
					return false;
				});
			});
</script>


	</div> <!-- footer -->

<!-- ************* END OF FOOTER ********************* -->

</div> <!-- bottom -->
<script type="text/javascript">
	$('.ajaxForm').each(function(){
		$(this).load($(this).attr('data-location'),$.parseJSON($(this).attr('data-post')) )
	});
</script>
      
<!--Start of Tawk.to Script--><script type="text/javascript"> var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date(); (function(){ var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];s1.async=true; s1.src='https://embed.tawk.to/647e309494cf5d49dc5bf441/1h26erhh8'; s1.charset='UTF-8'; s1.setAttribute('crossorigin','*'); s0.parentNode.insertBefore(s1,s0); })(); </script> <!--End of Tawk.to Script--></body>

<!-- Mirrored from www.quickfreightsolutions.com/contact by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 30 May 2023 19:43:59 GMT -->
</html>
