/* NAVIGATION SUPPORT */

function basename(path) {
   return path.split('/').reverse()[0]; // last part of url
}


$(document).ready( function() {
	//$("#debug").prepend("<p style='color:red;'>debug works</p>");

	// GLOBAL
	window.weSeeMobileMenu = ( $('#mobileMenu').is(':visible') );

      //drive silo navigation
      $("span.topSubA").on("click", function() {
            location.href = $(this).attr("href");
      });
      $("span.topGchildA").on("click", function() {
            location.href = $(this).attr("href");
      });
      $("span.sideA").on("click", function() {
            location.href = $(this).attr("href");
      });
      $("span.firstSideA").on("click", function() {
            location.href = $(this).attr("href");
      });
      $("span.sideSubA").on("click", function() {
            location.href = $(this).attr("href");
      });

	// reset sidemenu
	closeSideAll();

	// FUNCTIONS CLOSE ALL
	// close all topSub & topGchild, reset arrows
	function closeTopAll(){
		$('.topArrow').each(function(){
			$(this).parent('.topRowLI').removeClass('arrowClicked');
			$(this).html( arrow('topPlus') );
			$(this).next('.topSubUL').removeClass('rightAlign');
			$(this).next('.topSubUL').hide();
		});
		// special treatment for dynamically created content
		$('body').find('.subArrow').html( arrow('subPlus') );
		$('body').find('.topGchildUL').hide();
	}

	// close all side
	function closeSideAll(){
		$('.sideArrow').each(function(){
			$(this).html( arrow('sidePlus') );
			$(this).next('.sideSubUL').hide();
		});
		// unless...
		$('.sideArrow.showSideSub').each(function(){
			$(this).addClass('arrowClicked');
			$(this).html( arrow('sideMinus') );
			$(this).next('.sideSubUL').show();
		});
	}


	// IF RESIZED
	$(window).resize( function() {
		var NOWweSeeMobileMenu = ( $('#mobileMenu').is(':visible') );
		// if swap mobile|desktop, set to default states
		if(NOWweSeeMobileMenu != weSeeMobileMenu){
			if(NOWweSeeMobileMenu) $('#topMenu').hide();
			else $('#topMenu').show();

			window.weSeeMobileMenu = NOWweSeeMobileMenu; // set m
			closeTopAll();
			closeSideAll();
		}
		window.weSeeMobileMenu = NOWweSeeMobileMenu;
	});

	// FUNCTION TOPMENU RIGHTALIGN SUBMENU
	// if menu too far to the right, switch it to the left
	function rightAlign(topSubUL){
		if( $(topSubUL).length ){ // if this topSubUL exist
			$(topSubUL).removeClass('rightAlign'); // default
			// no need to rightalign mobileMenu
			if (weSeeMobileMenu==false) {
				var topRowLI_leftPos = $(topSubUL).parent('.topRowLI').offset().left;
				var topSubUL_width = $(topSubUL).outerWidth();

				var topSubUL_rightPos = $(window).width() - topRowLI_leftPos - topSubUL_width; // if negative then it overflowed browser right edge

				if(topSubUL_rightPos<0){
					$(topSubUL).addClass('rightAlign'); // right:0;
					//$(topSubUL).css('margin-left',topSubUL_rightPos+'px'); // alternative right browser side
				}
			}
		}
	}

	// TOPMENU HOVER : DESKTOP RIGHTALIGN SUBMENU & SUBARROW FIX
	$('.notMobile .topRowLI').hover( function() { // hover inFunction
		if (weSeeMobileMenu==false) {
			var topSubUL = $(this).children('.topSubUL');
			$(topSubUL).show();
			rightAlign(topSubUL);
		}
	}, function() { // hover outFunction
		if (weSeeMobileMenu==false) {
			closeTopAll(); // close all topSub & topGchild, reset arrows
		}
	});


	// TOPARROW CLICK : TABLET & MOBILE TOGGLE, TABLET RIGHTALIGN SUBMENU
	$('.topArrow').click(function() {
		// change arrow html
		$(this).html(function(i,h){
			return ( $(this).next('.topSubUL').is(':visible') ? arrow('topPlus') : arrow('topMinus') );
		});

		var topSubUL = $(this).attr('data-child'); // "#{$link}-child"
		if (weSeeMobileMenu) $(topSubUL).slideToggle(); // mobile sub slides up/down
		else $(topSubUL).toggle();

		$(this).parent('.topRowLI').toggleClass('arrowClicked');

		// TABLET ONLY clean up previously clicked
		if (weSeeMobileMenu==false) {
			$('.topArrow').each(function() {
				var other_topSubUL = $(this).attr('data-child');
				if(other_topSubUL != topSubUL) {
					$(other_topSubUL).css('display','none');
					$(this).parent('.topRowLI').removeClass('arrowClicked');
					$(this).html( arrow('topPlus') );
				}
			});
		}
		rightAlign(topSubUL);
	});

	// SUBARROW CLICK : ALL PLATFORMS
	$('body').on('click', '.subArrow', function () {
		// change arrow html
		$(this).html(function(i,h){
			return ( $(this).next('.topGchildUL').is(':visible') ? arrow('subPlus') : arrow('subMinus') );
		});
		$(this).next('.topGchildUL').slideToggle();
		$(this).parent('.topSubLI').toggleClass('arrowClicked');
	});


	function arrow(a){
		if(a=='topPlus') 		r = (weSeeMobileMenu) ? jqMobTopPlus	: jqTopPlus;
		else if(a=='topMinus') 	r = (weSeeMobileMenu) ? jqMobTopMinus	: jqTopMinus;
		else if(a=='subPlus')	r = (weSeeMobileMenu) ? jqMobSubPlus	: jqSubPlus;
		else if(a=='subMinus') 	r = (weSeeMobileMenu) ? jqMobSubMinus	: jqSubMinus;
		else if(a=='sidePlus')	r = (weSeeMobileMenu) ? jqMobSidePlus	: jqSidePlus;
		else if(a=='sideMinus') r = (weSeeMobileMenu) ? jqMobSideMinus	: jqSideMinus;
		return r;
	}


	// MOBILE MENU CLICK : TOGGLE
	$('#mobileMenu').click( function() {
		closeTopAll(); // close all topSub & topGchild, reset arrows
		$('#topMenu').toggle();
	});


	// SIDE MENU
	$('.sideArrow').click( function() {
		// change arrow html
		$(this).html(function(i,h){
			return ( $(this).next('.sideSubUL').is(':visible') ? arrow('sidePlus') : arrow('sideMinus') );
		});

		$(this).next('.sideSubUL').slideToggle('MenuBarSubmenuVisible');

		// change arrow html
		$(this).toggleClass('arrowClicked');
	});

	/**/
});
